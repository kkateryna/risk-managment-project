# Application of machine learning in algorithmic investment strategies on global stock markets

## Introduction 

Investing involves allocating capital to gain future financial benefits, expecting returns to exceed the initial investment. Individuals and institutions often invest in financial instruments, including securities traded on stock exchanges. Passive capital management uses stock indices, while active management involves selecting equity instruments to achieve higher returns through fundamental and technical analysis.

## Application of Machine Learning in Algorithmic Investment Strategies on Global Stock Markets

This research replicates the study [_Application of Machine Learning in Algorithmic Investment Strategies on Global Stock Markets_](https://www.sciencedirect.com/science/article/pii/S0275531923001782). It compares investment strategies based on technical analysis indicators (SMA, MACD, SO, RSI, WPR) and machine learning techniques (Neural Networks, K Nearest Neighbor, Regression Trees, Random Forests, Naïve Bayes, Bayesian GLMs, SVMs). These strategies are evaluated against passive buy-and-hold strategies using metrics like annualized return, standard deviation, maximum drawdown, Sharpe Ratio, and Information Ratio.