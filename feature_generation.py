def calculate_sma_signal(df, n):
    df['SMA'] = df['Price'].rolling(window=n).mean()
    df['SMA_signal'] = df['Price'] - df['SMA']
    df.drop(columns=['SMA'], inplace=True)
    return df


def calculate_macd_signal(df):
    # Calculate the 12-period EMA
    df['EMA12'] = df['Price'].ewm(span=12, adjust=False).mean()

    # Calculate the 26-period EMA
    df['EMA26'] = df['Price'].ewm(span=26, adjust=False).mean()

    # Calculate MACD (the difference between 12-period EMA and 26-period EMA)
    df['MACD'] = df['EMA26'] - df['EMA12']

    # Calculate the 9-period EMA (Signal Line)
    df['Signal_Line'] = df['Price'].ewm(span=9, adjust=False).mean()
    df['MACD_signal'] = df['MACD'] - df['Signal_Line']
    df.drop(columns=['EMA12', 'EMA26', 'MACD', 'Signal_Line'], inplace=True)
    return df


def get_stochastic_oscillator(df, period=14):
    df['best_low'] = df['Price'].rolling(window=period).min()
    df['best_high'] = df['Price'].rolling(window=period).max()
    df['fast_k'] = 100 * ((df['Price'] - df['best_low']) / (df['best_high'] - df['best_low']))
    df['fast_d'] = df['fast_k'].rolling(window=3).mean().round(2)
    df['slow_k'] = df['fast_d']
    df['slow_d'] = df['slow_k'].rolling(window=3).mean().round(2)

    return df


def get_rsi(df):
    change = df["Price"].diff()
    change.dropna(inplace=True)

    change_up = change.copy()
    change_down = change.copy()

    change_up[change_up < 0] = 0
    change_down[change_down > 0] = 0

    # Verify that we did not make any mistakes
    change.equals(change_up + change_down)

    # Calculate the rolling average of average up and average down
    avg_up = change_up.rolling(14).mean()
    avg_down = change_down.rolling(14).mean().abs()

    df['rsi'] = 100 * avg_up / (avg_up + avg_down)

    return df


def get_wpr(df):
    df['wpr'] = (df['best_high'] - df['Price']) / (df['best_high'] - df['best_low'])

    return df


def min_max_normalize(column):
    return (column - column.min()) / (column.max() - column.min())
