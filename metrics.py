import numpy as np

def cagr(df, cum_return):
  num_years = (df.index[-1] - df.index[200]).days / 365
  start_value = num_years
  end_value = df[cum_return].iloc[-1]*start_value


  CAGR = (abs((end_value / start_value)) ** (1 / num_years) - 1)*(1 if end_value >= 0 else -1)

  return CAGR


def adj_sr(df, cum_return, daily_return):
    cumulative_return = df[cum_return].iloc[-1]
    num_days = (df.index[-1] - df.index[200]).days

    annualized_return = ((1 + abs(cumulative_return)) ** (365 / num_days) - 1) * (1 if cumulative_return >= 0 else -1)
    annualized_std = daily_return.std() * np.sqrt(365)

    if annualized_std != 0:
        sharpe_ratio = annualized_return / annualized_std
    else:
        sharpe_ratio = 0

    adjusted_sharpe_ratio = max(sharpe_ratio, 0)

    return adjusted_sharpe_ratio

def calculate_mdd(returns_series):

  peak_to_trough = returns_series.cummax() - returns_series
  return peak_to_trough.max() * 100

def annualized_return(returns, periods_per_year):
    total_return = np.prod(1 + returns)
    annualized_return = total_return ** (periods_per_year / len(returns)) - 1
    return annualized_return

def downside_deviation(returns, target_return=0):
    downside_returns = [ret for ret in returns if ret < target_return]
    squared_downside_returns = [(ret - target_return) ** 2 for ret in downside_returns]
    downside_deviation = np.sqrt(np.mean(squared_downside_returns)) * np.sqrt(len(returns))
    return downside_deviation

def calculate_sortino_ratio(returns, periods_per_year):
    R = annualized_return(returns, periods_per_year)
    sigma_Rd = downside_deviation(returns)
    sortino_ratio = max(R, 0) / sigma_Rd
    return sortino_ratio

def calculate_information_ratio(returns, periods_per_year):
    R = max(annualized_return(returns, periods_per_year), 0) ** 2
    sigma_Rd = downside_deviation(returns)
    mdd = calculate_mdd(returns)
    return R / (sigma_Rd * mdd)
