from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPRegressor
import numpy as np
import statsmodels.api as sm


def rolling_window_regression(df, columns, target_column='discrete_return', window_size_in=200, window_size_out=20,
                              model_type='KNN', **kwargs):
    total_windows = (len(df) - window_size_in) // window_size_out

    for start in range(0, total_windows * window_size_out, window_size_out):
        end_in = start + window_size_in
        end_out = end_in + window_size_out

        if end_out > len(df):
            break

        X_train = df.iloc[start:end_in][columns]
        if model_type == 'Naive_Bayes':
            y_train = df.iloc[start:end_in]['target_NB']
        else:
            y_train = df.iloc[start:end_in][target_column]
        X_test = df.iloc[end_in:end_out][columns]

        if model_type == 'KNN':
            model = KNeighborsRegressor(**kwargs)
        elif model_type == 'Random_Forest':
            model = RandomForestRegressor(**kwargs)
        elif model_type == 'Decision_Tree':
            model = DecisionTreeRegressor(**kwargs)
        elif model_type == 'SVM_Linear':
            model = SVR(kernel='linear', **kwargs)
        elif model_type == 'SVM_Polynomial':
            model = SVR(kernel='poly', **kwargs)
        elif model_type == 'Naive_Bayes':
            model = GaussianNB()
        else:
            raise ValueError("Invalid model type")

        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)

        q40 = np.quantile(y_pred, 0.40)
        q60 = np.quantile(y_pred, 0.60)
        signals = np.where(y_pred >= q60, 1, np.where(y_pred <= q40, -1, 0))

        df.iloc[end_in:end_out, df.columns.get_loc(model_type)] = signals if not model_type == 'Naive_Bayes' else y_pred

        df['cumulative_return_' + model_type] = (df['discrete_return'] * df[model_type]).cumsum()


def train_bglm(X_train, y_train):
    X_train = sm.add_constant(X_train)

    model = sm.GLM(y_train > 0, X_train, family=sm.families.Binomial())
    result = model.fit()
    return result


def rolling_window_bglm_regression(df, columns, target_column='discrete_return', window_size_in=200,
                                   window_size_out=20):
    total_windows = (len(df) - window_size_in) // window_size_out

    for start in range(0, total_windows * window_size_out, window_size_out):
        end_in = start + window_size_in
        end_out = end_in + window_size_out

        if end_out > len(df):
            break

        X_train = df.iloc[start:end_in][columns]
        y_train = df.iloc[start:end_in][target_column]
        X_test = df.iloc[end_in:end_out][columns]

        X_test = sm.add_constant(X_test)
        bglm_model = train_bglm(X_train, y_train)

        y_pred = bglm_model.predict(X_test)

        q40 = np.quantile(y_pred, 0.40)
        q60 = np.quantile(y_pred, 0.60)

        signals = np.where(y_pred >= q60, 1, np.where(y_pred <= q40, -1, 0))

        df.iloc[end_in:end_out, df.columns.get_loc("BGLM_Logit")] = signals

        df['cumulative_return_' + "BGLM_Logit"] = (df['discrete_return'] * df["BGLM_Logit"]).cumsum()


def train_elm(X_train, y_train, activation='tanh'):
    input_neurons = X_train.shape[1]
    best_neurons = 0
    best_score = -np.inf

    for neurons in range(1, 2 * input_neurons + 1):
        elm_model = MLPRegressor(hidden_layer_sizes=(neurons,),
                                 activation=activation,
                                 solver='lbfgs',
                                 random_state=42)
        elm_model.fit(X_train, y_train)
        score = elm_model.score(X_train, y_train)
        if score > best_score:
            best_score = score
            best_neurons = neurons

    best_elm_model = MLPRegressor(hidden_layer_sizes=(best_neurons,),
                                  activation=activation,
                                  solver='lbfgs',
                                  random_state=42)
    best_elm_model.fit(X_train, y_train)
    return best_elm_model


def rolling_window_elm_regression(df, columns, target_column='discrete_return', window_size_in=200, window_size_out=20,
                                  activation='tanh'):
    total_windows = (len(df) - window_size_in) // window_size_out

    for start in range(0, total_windows * window_size_out, window_size_out):
        end_in = start + window_size_in
        end_out = end_in + window_size_out

        if end_out > len(df):
            break

        X_train = df.iloc[start:end_in][columns]
        y_train = df.iloc[start:end_in][target_column]
        X_test = df.iloc[end_in:end_out][columns]

        elm_model = train_elm(X_train, y_train, activation=activation)

        y_pred = elm_model.predict(X_test)

        q40 = np.quantile(y_pred, 0.40)
        q60 = np.quantile(y_pred, 0.60)

        signals = np.where(y_pred >= q60, 1, np.where(y_pred <= q40, -1, 0))

        df.iloc[end_in:end_out, df.columns.get_loc('ELM_' + activation)] = signals

        df['cumulative_return_ELM_' + activation] = (df['discrete_return'] * df['ELM_' + activation]).cumsum()
